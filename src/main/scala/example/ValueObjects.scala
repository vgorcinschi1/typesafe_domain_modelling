package example
import cats.implicits._

object ValueObjects {

  case class Username private (val value: String) extends AnyVal

  case class Email private (val value: String) extends AnyVal

  def mkUsername(value: String): Option[Username] =
    (value.nonEmpty).guard[Option].as(Username(value))

  def mkEmail(value: String): Option[Email] =
    (value.contains("@")).guard[Option].as(Email(value))

}

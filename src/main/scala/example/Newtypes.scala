package example

import io.estatico.newtype.macros._

object Newtypes {
  @newtype case class Username(value: String)

  @newtype case class Email(value: String)

}

package example

import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.Contains
import eu.timepit.refined.types.string.NonEmptyString

object RefinementTypes {

  type Username = NonEmptyString
  type Email = String Refined Contains['@']
}

package example

import scala.util.control.NoStackTrace
import cats.data.{EitherNel, ValidatedNel}
import cats.implicits._
import eu.timepit.refined._
import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.collection.Contains
import eu.timepit.refined.numeric.Greater
import eu.timepit.refined.types.string.NonEmptyString
import example.RuntimeValidation.{Email, Username}
import io.estatico.newtype.macros._

object RuntimeValidation {

  // for existing
  //  def toNonEmptyString(string: String): Either[String, NonEmptyString] = NonEmptyString.from(string)

  // for custom
  type GTFive = Int Refined Greater[5]

  object GTFive extends RefinedTypeOps[GTFive, Int]

  //  def toGTFive(int: Int): Either[String, GTFive] = GTFive.from(int)

  // final iteration on Username, Email...
  type UsernameR = NonEmptyString

  object UsernameR extends RefinedTypeOps[UsernameR, String]

  type EmailR = String Refined Contains['@']

  object EmailR extends RefinedTypeOps[EmailR, String]

  @newtype case class Username(value: UsernameR)

  @newtype case class Email(value: EmailR)

  //  final case class User private(username: Username, email: Email)
  //
  //  object User {
  //    def mkUser(u: String, e: String): EitherNel[String, User] = (
  //      UsernameR.from(u).toEitherNel.map(Username.apply),
  //      EmailR.from(e).toEitherNel.map(Email.apply)
  //      ).parMapN(User.apply)
  //  }
}

// ...and remove boilerplate
object NewtypeRefinedOps {

  import io.estatico.newtype.Coercible
  import io.estatico.newtype.ops._

  final class NewtypePartiallyApplied[A, T](raw: T) {
    def validate[P](implicit c: Coercible[Refined[T, P], A], v: Validate[T, P]): EitherNel[String, A] =
      refineV[P](raw).toEitherNel.map(_.coerce[A])
  }

  implicit class NewtypeOps[T](raw: T) {
    def as[A]: NewtypePartiallyApplied[A, T] = new NewtypePartiallyApplied[A, T](raw)
  }

  final class NewtypeRefinedPartiallyApplied[A] {
    def apply[T, P](raw: T)(implicit c: Coercible[Refined[T, P], A], v: Validate[T, P]): EitherNel[String, A] =
      refineV[P](raw).toEitherNel.map(_.coerce[A])
  }

  def validate[A]: NewtypeRefinedPartiallyApplied[A] = new NewtypeRefinedPartiallyApplied[A]

//  final case class User private(username: Username, email: Email)
//
//  object User {
//    def mkUser(u: String, e: String): EitherNel[String, User] = (
//      u.as[Username].validate,
//      e.as[Email].validate
//      ).parMapN(User.apply)
//  }
}

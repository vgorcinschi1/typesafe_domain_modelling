import sbt._

object Dependencies {
  lazy val catsVersion = "2.6.1"
  lazy val newtypeVersion = "0.4.4"
  lazy val refinedVersion = "0.9.27"

  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.9"

  val compileDependencies = Seq(
    "org.typelevel" %% "cats-core" % catsVersion,
    "eu.timepit" %% "refined" % refinedVersion,
    "io.estatico" %% "newtype" % newtypeVersion
  )

  val testDependencies = Seq(
    scalaTest % Test
  )
}
